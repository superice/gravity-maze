# Gravity Maze
Name still subject to change

[Try it here](http://node1.ricklubbers.nl/gravitymaze/)

## How to play
Goal of the game: remove all green blocks by letting them touch the brown block.
Turn the field by using the arrow keys or the buttons on screen.

## Running locally
Clone the repository, `yarn`, `yarn dev` and you should be good to go.

## Contributing
We need more levels! See `src/elm/Levels.elm` for the level definition format.

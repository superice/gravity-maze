import './css/style.css';
import Elm from './elm/Main.elm';

const game = Elm.Main.embed(document.getElementById('elm-main'));

window.flipSquare = function(x, y) {
    game.ports.setSquare.send(x, y);
}

module Link exposing (..)

import Html exposing (Html, Attribute, node)
import Messages exposing (Msg)

link : List (Attribute Msg) -> Html Msg
link attributes =
    node "link" attributes []

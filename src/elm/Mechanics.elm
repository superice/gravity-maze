module Mechanics exposing (checkWin)

import Fields exposing (Field (..), Level, Row)
import List exposing (foldl)

checkWin : Level -> Bool
checkWin rows =
    not (rowsContainsBlock rows)

rowsContainsBlock : Level -> Bool
rowsContainsBlock rows =
    foldl (\col acc -> (columnContainsBlock col) || acc) False rows


columnContainsBlock : Row -> Bool
columnContainsBlock column =
    foldl (\elem acc -> elem == Block || acc) False column

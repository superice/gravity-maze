module Levels exposing (..)

import Fields exposing (Field (..), Level)

levels : List Level
levels = [
        [ [ Empty, Empty, Empty, Empty, Empty ]
        , [ Block, Empty, Empty, Empty, Empty ]
        , [ Wall,  Wall,  Empty, Empty, Empty ]
        , [ Empty, Empty, Empty, Empty, Wall  ]
        , [ Empty, Empty, Out,   Empty, Empty ]
        ]
        ,
        [ [ Empty, Empty, Empty, Empty, Empty ]
        , [ Block, Empty, Empty, Empty, Empty ]
        , [ Wall,  Empty, Empty, Empty, Empty ]
        , [ Empty, Empty, Empty, Empty, Wall  ]
        , [ Empty, Empty, Out,   Empty, Empty ]
        ]
        ,
        [ [ Block, Empty, Empty, Empty, Empty, Wall  ]
        , [ Wall,  Empty, Empty, Empty, Empty, Wall  ]
        , [ Empty, Empty, Empty, Empty, Empty, Empty ]
        , [ Empty, Empty, Empty, Empty, Wall,  Empty ]
        , [ Wall,  Empty, Out,   Empty, Wall,  Empty ]
        ]
        ,
        [ [ Empty, Block, Empty, Wall  ]
        , [ Empty, Wall,  Empty, Wall  ]
        , [ Empty, Empty, Empty, Empty ]
        , [ Empty, Empty, Wall,  Empty ]
        , [ Wall,  Empty, Empty, Empty ]
        , [ Wall,  Out,   Wall,  Wall  ]
        ]
        ,
        [ [ Block, Empty, Empty, Wall,  Empty, Empty ]
        , [ Wall,  Empty, Empty, Empty, Empty, Wall  ]
        , [ Wall,  Empty, Wall,  Empty, Empty, Empty ]
        , [ Empty, Empty, Empty, Wall,  Wall,  Empty ]
        , [ Empty, Wall,  Empty, Empty, Out,   Empty ]
        , [ Empty, Empty, Empty, Wall,  Empty, Empty ]
        ]
        ,
        [ [ Block, Empty, Empty, Wall,  Empty, Empty ]
        , [ Wall,  Empty, Block, Empty, Empty, Wall  ]
        , [ Wall,  Empty, Wall,  Empty, Empty, Empty ]
        , [ Empty, Empty, Empty, Wall,  Wall,  Empty ]
        , [ Empty, Wall,  Empty, Empty, Out,   Empty ]
        , [ Empty, Empty, Empty, Wall,  Empty, Empty ]
        ]
        -- ,
        -- [ [ Block, Empty, Empty, Wall,  Empty, Empty ]
        -- , [ Wall,  Empty, Empty, Empty, Empty, Wall  ]
        -- , [ Wall,  Empty, Wall,  Wall, Empty, Empty ]
        -- , [ Block, Empty, Empty, Empty,  Wall,  Empty ]
        -- , [ Empty, Out,  Wall, Empty, Empty,   Empty ]
        -- , [ Empty, Empty, Empty, Empty,  Wall, Wall ]
        -- ]
    ]

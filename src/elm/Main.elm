port module Main exposing (..)

import Html exposing (Html, button, div, text)
import Html.Events exposing (onClick)
import Html.Attributes exposing (rel, href)
import Keyboard
import List exposing (head, tail, length)
import Maybe exposing (withDefault)
import Link exposing (link)
import Messages exposing (Msg (..))
import Fields exposing (Field (..), fieldsView, Level)
import Gravity exposing (..)
import Mechanics exposing (..)
import Controls exposing (..)
import Scoring exposing (..)
import Levels

main =
    Html.program
        { init = (model, Cmd.none)
        , update = (\msg model -> (update msg model, Cmd.none))
        , subscriptions = subscriptions
        , view = view
        }

-- Model
type alias Model =
    { direction: Int
    , moves: Int
    , movesTotal: Int
    , level: Int
    , levels: List Level
    , fields: Level
    , win: Bool
    }

model : Model
model =
  { direction = 0
  , moves = 0
  , movesTotal = 0
  , level = 1
  , levels = Levels.levels
  , fields = (withDefault [] (head Levels.levels))
  , win = False
  }

-- Update
update : Msg -> Model -> Model
update msg model =
    case msg of
        Counterclockwise ->
            case model.win of
                False -> { model |
                              moves = model.moves + 1
                            , movesTotal = model.movesTotal + 1
                            , direction = model.direction - 90
                            , fields = apply (model.direction - 90) model.fields
                            , win = checkWin (apply (model.direction - 90) model.fields)
                            }
                True -> model
        Clockwise ->
            case model.win of
                False -> { model |
                              moves = model.moves + 1
                            , movesTotal = model.movesTotal + 1
                            , direction = model.direction + 90
                            , fields = apply (model.direction + 90) model.fields
                            , win = checkWin (apply (model.direction + 90) model.fields)
                            }
                True -> model
        KeyMsg code ->
            case code of
                37 -> update Counterclockwise model
                39 -> update Clockwise model
                82 -> update ResetLevel model
                78 -> update NextLevel model
                _ -> model
        NextLevel ->
            case (model.win && (length model.levels) > 1) of
                True -> { model |
                              moves = 0
                            , direction = 0
                            , level = model.level + 1
                            , levels = withDefault [] (tail model.levels)
                            , fields = withDefault [] (head (withDefault [] (tail model.levels)))
                            , win = False
                            }
                False -> model
        ResetLevel ->
            { model |
                  moves = 0
                , direction = 0
                , fields = withDefault [] (head model.levels)
                , win = False
            }

-- Subscriptions
subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch [ Keyboard.ups KeyMsg ]

-- View
view : Model -> Html Msg
view model =
    div [] [ link [ rel "stylesheet", href "/style.css" ]
        , renderControls model.levels model.win
        , renderScore model.level model.moves model.movesTotal
        , div [] [
            fieldsView model.direction model.win model.fields
        ]]

module Gravity exposing (..)

import Fields exposing (Field (..), Row, Level)
import List exposing (reverse, map, map2, tail)

apply : Int -> Level -> Level
apply dir rows = rotate (90 - dir) (applyGravityUntilConstant (rotate (270 + dir) rows))

rotate : Int -> Level -> Level
rotate dir rows =
    case (dir % 360) of
        0 -> rows
        90 -> transpose (reverse rows)
        180 -> reverse (map (reverse) rows)
        270 -> transpose (map (reverse) rows)
        _ -> rows

applyGravityUntilConstant : Level -> Level
applyGravityUntilConstant rows =
    case (applyGravity rows) == rows of
        True -> rows
        False -> applyGravityUntilConstant (applyGravity rows)

applyGravity : Level -> Level
applyGravity rows = map (singleRow []) rows

transpose ll =
    let heads = List.map (List.take 1) ll |> List.concat
        tails = List.map (List.drop 1) ll
    in
        if List.isEmpty heads
            then []
            else heads::(transpose tails)

singleRow : Row -> Row -> Row
singleRow prepend list =
    case list of
        (Block::Empty::xs) -> singleRow [] (prepend ++ [ Empty, Block ] ++ xs)
        (Block::Out::xs) -> singleRow [] (prepend ++ [ Empty, Out ] ++ xs)
        (x::y::xs) -> singleRow (prepend ++ [ x ]) (y::xs)
        (x::xs) -> prepend ++ [ x ]
        [] -> prepend

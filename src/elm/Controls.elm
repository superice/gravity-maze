module Controls exposing (..)

import Html exposing (Html, Attribute, div, text, button)
import Html.Attributes exposing (class, style)
import Html.Events exposing (onClick)
import List exposing (map, length, tail)
import Messages exposing (Msg (..))
import Fields exposing (Field (..), Level)

renderControls : List (Level) -> Bool -> Html Msg
renderControls levels win =
    case win of
        True -> case (length levels) of
            1 -> div
                [ class "controls" ] [ button [ onClick ResetLevel ] [ text "Retry" ] ]
            _ -> div
                [ class "controls" ]
                [ button [ onClick ResetLevel ] [ text "Retry" ]
                , button [ onClick NextLevel ] [ text "Next Level" ]
                ]
        False -> div
                 [ class "controls" ]
                 [ button [ onClick Counterclockwise ] [ text "Counterclockwise" ]
                 , button [ onClick Clockwise ] [ text "Clockwise" ]
                 , button [ onClick ResetLevel ] [ text "Retry" ]
                 ]

module Fields exposing (Field (..), fieldsView, Row, Level)

import Html exposing (Html, Attribute, div, text)
import Html.Attributes exposing (class, style)
import List exposing (map)
import Messages exposing (Msg)

type Field = Empty | Wall | Block | Out
type alias Row = List Field
type alias Level = List Row

classNameFieldType : Field -> Attribute Msg
classNameFieldType field = case field of
    Empty -> class "field-type-empty"
    Wall -> class "field-type-wall"
    Block -> class "field-type-block"
    Out -> class "field-type-out"

fieldClassName : Bool -> Attribute Msg
fieldClassName win =
    case win of
        True -> class "field field-win"
        False -> class "field"

rotateStyle : Bool -> Int -> Attribute Msg
rotateStyle win dir =
    case win of
        False -> style [ ("transform", ("rotateZ(" ++ (toString dir) ++ "deg)")) ]
        True -> style [ ("transform", "rotateZ(0deg)") ]

colView : Field -> Html Msg
colView field =
    div [ class "field-item", classNameFieldType field ] []

rowView : Row -> Html Msg
rowView cols =
    div [ class "field-row" ] (map colView cols)

fieldsView : Int -> Bool -> Level -> Html Msg
fieldsView dir win rows =
    div [ class "field-wrapper" ] [
        div [ fieldClassName win
            , rotateStyle win dir
            ]
            (map rowView rows)
        ]

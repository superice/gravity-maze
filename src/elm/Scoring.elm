module Scoring exposing (..)

import Html exposing (Html, div, text)
import Html.Attributes exposing (class)
import Messages exposing (Msg (..))

renderScore : Int -> Int -> Int -> Html Msg
renderScore level moves movesTotal =
    div [ class "score" ]
        [ text "Level "
        , text (toString level)
        , text " - "
        , text (toString moves)
        , text " moves"
        , text " - "
        , text (toString movesTotal)
        , text " total"
        ]

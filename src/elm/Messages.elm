module Messages exposing (..)

import Keyboard

type Msg
    = Counterclockwise
    | Clockwise
    | NextLevel
    | ResetLevel
    | KeyMsg Keyboard.KeyCode
